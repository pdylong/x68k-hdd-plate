# HDD Mounting plate for the Sharp X68000

X68k Mounting plate for the HENKAN BANCHO PRO and other.

![x68k-hdd-plate](x68k-hdd-plate.svg)

![x68k-hdd-plate prepared](plate-prepared.jpeg)

![x68k-hdd-plate installed](plate-installed.jpeg)